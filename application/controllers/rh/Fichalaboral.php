<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fichalaboral extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata("login")) {
			redirect(base_url());
		}
		$this->load->model("Categorias_model");
	}

	
	public function index()
	{
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/fichalaboral/create");
		$this->load->view("layouts/footer", array("script" => "admin/fichalaboral/javascript"));
	}
}
