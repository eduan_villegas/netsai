        <footer class="main-footer">
        	<div class="pull-right hidden-xs">
        		<b>Version</b> 1.0.0
        	</div>
        	<strong>Copyright &copy; 2019 Telnycs S.A de C.V.</strong> All rights
        	reserved.
        </footer>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->
        <script src="<?php echo base_url(); ?>assets/template/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/jquery-print/jquery.print.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url(); ?>assets/template/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/jquery-ui/jquery-ui.js"></script>
        <!-- Select2 -->
        <script src="<?php echo base_url(); ?>assets/template/select2/dist/js/select2.full.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>assets/template/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>assets/template/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <!-- DataTables Export -->
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/r-2.2.0/sl-1.2.3/datatables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/datatables/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/datatables/buttons.html5.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/datatables/buttons.colVis.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/datatables/jszip.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/datatables/pdfmake.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/datatables/vfs_fonts.js"></script>

        <!--<script src="<?php echo base_url(); ?>assets/template/datatables-export/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/datatables-export/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/datatables-export/js/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/datatables-export/js/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/datatables-export/js/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/template/datatables-export/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/datatables-export/js/buttons.print.min.js"></script>-->
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>assets/template/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/template/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>assets/template/dist/js/demo.js"></script>
        <script src="<?php echo base_url(); ?>assets/template/fullcalendar/lib/moment.min.js"></script>
        <!-- datepicker -->
        <script type="text/javascript" src="<?php echo base_url() . 'assets/template/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/template/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script>
        <!-- bootstrap datepicker -->
        <script type="text/javascript" src="<?php echo base_url() . 'assets/template/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/template/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js'; ?>"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url() . 'assets/template/bootstrap-timepicker/bootstrap-timepicker.min.js'; ?>"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url(); ?>assets/template/bootstrap-sweetalert/dist/sweetalert.js"></script>
        <!-- fullcalendar -->
        <script type="text/javascript" src="<?php echo base_url() . 'assets/template/fullcalendar/fullcalendar.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/template/fullcalendar/locale/es.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/template/fullcalendar/gcal.js'; ?>"></script>
        <!--<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>-->
        <!--<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/responsive/1.0.2/js/dataTables.responsive.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>-->
        <?php if (isset($script)) $this->load->view($script); ?>
        <script>
        	$(document).ready(function() {
        		var base_url = "<?php echo base_url(); ?>";
        		$(document).on("click", "#btn-print", function() {
        			$("#body1").print({
        				title: "Comprobante de Venta"
        			});
        		});
        		$('.select2').select2();
        		//Date picker
        		$('.datepicker').datepicker({
        			format: 'yyyy/mm/dd',
        			autoclose: true,
        			language: 'es',
        		})

        		//Timepicker
        		$('.timepicker').timepicker({
        			showInputs: false,
        		})

        		$('#example').DataTable({
        			dom: 'Bfrtip',
        			buttons: [{
        					extend: 'excelHtml5',
        					title: "Listado de Ventas",
        					exportOptions: {
        						columns: [0, 1, 2, 3, 4, 5]
        					}
        				},
        				{
        					extend: 'pdfHtml5',
        					title: "Listado de Ventas",
        					exportOptions: {
        						columns: [0, 1, 2, 3, 4, 5]
        					}

        				}
        			],

        			language: {
        				"lengthMenu": "Mostrar _MENU_ registros por pagina",
        				"zeroRecords": "No se encontraron resultados en su busqueda",
        				"searchPlaceholder": "Buscar registros",
        				"info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
        				"infoEmpty": "No existen registros",
        				"infoFiltered": "(filtrado de un total de _MAX_ registros)",
        				"search": "Buscar:",
        				"paginate": {
        					"first": "Primero",
        					"last": "Último",
        					"next": "Siguiente",
        					"previous": "Anterior"
        				},

        			}
        		});

        		$('#example1').DataTable({
        			dom: 'Bfrtip',
        			buttons: [
        				'copyHtml5',
        				'excelHtml5',
        				'csvHtml5',
        				'pdfHtml5'
        			],
        			"responsive": true,
        			"language": {
        				"lengthMenu": "Mostrar _MENU_ registros por pagina",
        				"zeroRecords": "No se encontraron resultados en su busqueda",
        				"searchPlaceholder": "Buscar registros",
        				"info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
        				"infoEmpty": "No existen registros",
        				"infoFiltered": "(filtrado de un total de _MAX_ registros)",
        				"search": "Buscar:",
        				"paginate": {
        					"first": "Primero",
        					"last": "Último",
        					"next": "Siguiente",
        					"previous": "Anterior"
        				},
        			}
        		});
        		$('.sidebar-menu').tree();
        	})
        </script>
        </body>

        </html>
