<script>
  var base_url = "<?php echo base_url(); ?>";

  function abrirchat(id) {
    $.ajax({
      type: "POST",
      url: base_url + "tickets/MantenimientoController/viewajax",
      dataType: 'json',
      data: {
        'id': id
      },
      success: function(data) {
        $('#asunto').val(data.asunto);
        $('#descripcion').val(data.descripcion);
        $('#idusuario').val(data.idusuario);
        $('#idmanto').val(data.idmantenimiento);
				$('#correousu').val(data.usucorreo);
        //alert(correo);
        $('#modalchat').modal('show');
      }
    }).fail(function(xhr, status, error) {
      console.log(xhr);
      console.log(status);
      console.log(error);
    })
  }
</script>
