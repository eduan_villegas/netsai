<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Usuarios
            <small>Editar</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($this->session->flashdata("error")) : ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>

                            </div>
                        <?php endif; ?>
                        <form action="<?php echo base_url(); ?>usuarios/usuarios/update" method="POST" enctype="multipart/form-data">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Nombres:</label>
                                <input type="text" class="form-control" id="nomusu" name="nomusu" value="<?php echo $usuarios->usuNombre; ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Apellido Paterno:</label>
                                <input type="text" class="form-control" id="pateruno" name="pateruno" value="<?php echo $usuarios->usuPaterno; ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Apellido Materno:</label>
                                <input type="text" class="form-control" id="materusu" name="materusu" value="<?php echo $usuarios->usuMaterno; ?>">
                            </div>
                            <div class="form-group <?php echo !empty(form_error('usucur')) ? 'has-error' : ''; ?> col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                <label> Curp:</label>
                                <input type="text" class="form-control" id="usucur" name="usucur" value="<?php echo !empty(form_error('usucur')) ? set_value('usucur') : $usuarios->usucurp ?>">
                                <?php echo form_error("usucur", "<span class='help-block'>", "</span>"); ?>
                            </div>
                            <div class="form-group <?php echo !empty(form_error('usucur')) ? 'has-error' : '' ?> col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                <label> RFC:</label>
                                <input type="text" class="form-control" id="usurfc" name="usurfc" value="<?php echo !empty(form_error('usurfc')) ? set_value('usurfc') : $usuarios->usurfc ?>">
                                <?php echo form_error("usurfc", "<span class='help-block'>", "</span>"); ?>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Dirección:</label>
                                <input type="text" class="form-control" id="usudire" name="usudire" value="<?php echo $usuarios->usudire ?>">
                            </div>
                            <div class="form-group  col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                <label> Telefono:</label>
                                <input type="text" class="form-control" id="usutel" name="usutel" value="<?php echo $usuarios->usutel ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Celular:</label>
                                <input type="text" class="form-control" id="usucel" name="usucel" value="<?php echo $usuarios->usucel ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Email Personal:</label>
                                <input type="email" class="form-control" id="usucorr" name="usucorr" value="<?php echo $usuarios->usumail ?>">
                            </div>
                            <div class="form-group <?php echo !empty(form_error('usunss')) ? 'has-error' : '' ?> col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Nss:</label>
                                <input type="text" class="form-control" id="usunss" name="usunss" value="<?php echo $usuarios->usunss ?>">
                                <?php echo form_error("usunss", "<span class='help-block'>", "</span>"); ?>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Lugar de Nacimiento:</label>
                                <input type="text" class="form-control" id="usunac" name="usunac" value="<?php echo $usuarios->usulug ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Funcion de Usuario:</label>
                                <input type="text" class="form-control" id="usufun" name="usufun" value="<?php echo $usuarios->usufun ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Rol de Usuario:</label>
                                <input type="text" class="form-control" id="usurol" name="usurol" value="<?php echo $usuarios->usurol ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Tipo de Sangre:</label>
                                <input type="text" class="form-control" id="ususan" name="ususan" value="<?php echo $usuarios->ususan ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Alergia a algun Medicamento:</label>
                                <input type="text" class="form-control" id="usuale" name="usuale" value="<?php echo $usuarios->usualerg ?>">
                            </div>
                            <div class="form-group <?php echo !empty(form_error('usucur')) ? 'has-error' : '' ?> col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Usuario:</label>
                                <input type="text" class="form-control" id="usuusu" name="usuusu" value="<?php echo !empty(form_error('usuusu')) ? set_value('usuusu') : $usuarios->usuario ?>">
                                <?php echo form_error("usuusu", "<span class='help-block'>", "</span>"); ?>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Password:</label>
                                <input type="password" class="form-control" id="passusu" name="passusu" value="<?php echo $usuarios->password ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Email:</label>
                                <input type="email" class="form-control" id="correousu" name="correousu" value="<?php echo $usuarios->usucorreo ?>">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Estatus:</label>
                                <select name="estatusu" id="estatusu" class="form-control" required="required">
                                    <?php if ($usuarios->usuActivo == 1) : ?>
                                        <option value="1" selected>ACTIVO</option>
                                        <option value="0">DESACTIVADO</option>
                                    <?php else : ?>
                                        <option value="1">ACTIVO</option>
                                        <option value="0" selected>DESACTIVADO</option>
                                    <?php endif; ?>

                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Perfiles:</label>
                                    <select name="perfilusu" id="perfilusu" class="form-control">
                                        <?php foreach ($perfiles as $perfil) : ?>
                                            <?php if ($perfil->idPerfil == $usuarios->idPerfil) : ?>
                                                <option value="<?php echo $perfil->idPerfil ?>" selected><?php echo $perfil->perNombre; ?></option>
                                            <?php else : ?>
                                                <option value="<?php echo $perfil->idPerfil ?>"><?php echo $perfil->perNombre; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label> Folio:</label>
                                    <input type="text" name="folio" id="folio" class="form-control" value="<?php echo $usuarios->folio ?>" readonly>
                                </div>
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Empresas:</label>
                                <select name="empreusu" id="empreusu" class="form-control">
                                    <?php foreach ($empresas as $empresa) : ?>
                                        <?php if ($empresa->idEmpresa == $usuarios->idEmpresa) : ?>
                                            <option value="<?php echo $empresa->idEmpresa ?>" selected><?php echo $empresa->nombreEmpresa; ?></option>
                                        <?php else : ?>
                                            <option value="<?php echo $empresa->idEmpresa ?>"><?php echo $empresa->nombreEmpresa; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Areas:</label>
                                <select name="areausu" id="areausu" class="form-control">
                                    <?php foreach ($areas as $area) : ?>
                                        <?php if ($area->idArea == $usuarios->idArea) : ?>
                                            <option value="<?php echo $area->idArea ?>" selected><?php echo $area->nombreArea; ?></option>
                                        <?php else : ?>
                                            <option value="<?php echo $area->idArea ?>"><?php echo $area->nombreArea; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Imagen:</label>
                                <input type="file" class="form-control" id="imagen" name="imagen">
                                <input type="hidden" name="imagenactual" id="imagenactual">
                                <img src="<?php echo base_url() ?>assets/usuarios/imagenes/<?php echo $usuarios->imagen; ?>" width="150px" height="150px" id="imagenmuestra">
                            </div>
                            <?php
                            //Declaramos el array para almacenar todos los permisos marcados
                            $valores = array();

                            //Almacenar los permisos asignados al usuario en el array
                            foreach ($usuper as $per) {
                                array_push($valores, $per->idPermiso);
                            }
                            ?>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Permisos:</label><br>
                                <?php foreach ($permisos as $permiso1) : ?>
                                    <?php if (in_array($permiso1->idpermiso, $valores)) : ?>
                                        <input type="checkbox" class="form-check-input" name="permiso[]" value="<?php echo $permiso1->idpermiso; ?>" checked> <?php echo $permiso1->permiso; ?><br>
                                    <?php else : ?>
                                        <input type="checkbox" class="form-check-input" name="permiso[]" value="<?php echo $permiso1->idpermiso; ?>"> <?php echo $permiso1->permiso; ?><br>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                            </div>
                            <div class="form-group">
                                <input type="hidden" name="idusuario" value="<?php echo $usuarios->idUsuario ?>">
                                <button type="submit" class="btn btn-success btn-flat"><span class="fa fa-save"> Guardar</span></button>
                                <a class="btn btn-danger btn-flat" href="<?php echo base_url(); ?>usuarios/usuarios"><span class="fa fa-arrow-circle-left"> Cancelar</span></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->