<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Usuarios
            <small>Nuevo</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($this->session->flashdata("error")) : ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error"); ?></p>
                            </div>
                        <?php endif; ?>
                        <form action="<?php echo base_url(); ?>usuarios/usuarios/store" method="POST" enctype="multipart/form-data">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Nombres:</label>
                                <input type="text" class="form-control" id="nomusu" name="nomusu">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Apellido Paterno:</label>
                                <input type="text" class="form-control" id="pateruno" name="pateruno">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Apellido Materno:</label>
                                <input type="text" class="form-control" id="materusu" name="materusu">
                            </div>
                            <div class="form-group <?php echo form_error('usucur') == true ? 'has-error' : '' ?> col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                <label> Curp:</label>
                                <input type="text" class="form-control" id="usucur" name="usucur">
                                <?php echo form_error("usucur", "<span class='help-block'>", "</span>"); ?>
                            </div>
                            <div class="form-group <?php echo form_error('usurfc') == true ? 'has-error' : '' ?> col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                <label> RFC:</label>
                                <input type="text" class="form-control" id="usurfc" name="usurfc">
                                <?php echo form_error("usurfc", "<span class='help-block'>", "</span>"); ?>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Dirección:</label>
                                <input type="text" class="form-control" id="usudire" name="usudire">
                            </div>
                            <div class="form-group  col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                <label> Telefono:</label>
                                <input type="text" class="form-control" id="usutel" name="usutel">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Celular:</label>
                                <input type="text" class="form-control" id="usucel" name="usucel">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Email Personal:</label>
                                <input type="email" class="form-control" id="usucorr" name="usucorr">
                            </div>
                            <div class="form-group <?php echo form_error('usunss') == true ? 'has-error' : '' ?> col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Nss:</label>
                                <input type="text" class="form-control" id="usunss" name="usunss">
                                <?php echo form_error("usunss", "<span class='help-block'>", "</span>"); ?>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Lugar de Nacimiento:</label>
                                <input type="text" class="form-control" id="usunac" name="usunac">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Funcion de Usuario:</label>
                                <input type="text" class="form-control" id="usufun" name="usufun">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Rol de Usuario:</label>
                                <input type="text" class="form-control" id="usurol" name="usurol">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Tipo de Sangre:</label>
                                <input type="text" class="form-control" id="ususan" name="ususan">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Alergia a algun Medicamento:</label>
                                <input type="text" class="form-control" id="usuale" name="usuale">
                            </div>
                            <div class="form-group <?php echo form_error('usuusu') == true ? 'has-error' : '' ?> col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Usuario:</label>
                                <input type="text" class="form-control" id="usuusu" name="usuusu">
                                <?php echo form_error("usuusu", "<span class='help-block'>", "</span>"); ?>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Password:</label>
                                <input type="password" class="form-control" id="passusu" name="passusu">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Email:</label>
                                <input type="email" class="form-control" id="correousu" name="correousu">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Estatus:</label>
                                <select name="estatusu" id="estatusu" class="form-control" required="required">
                                    <option value="">Estatus..</option>
                                    <option value="1">ACTIVO</option>
                                    <option value="0">DESACTIVADO</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Perfiles:</label>
                                    <select name="perfilusu" id="perfilusu" class="form-control">
                                        <?php foreach ($perfiles as $perfil) : ?>
                                            <option value="<?php echo $perfil->idPerfil ?>"><?php echo $perfil->perNombre; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label> Folio:</label>
                                    <input type="text" name="folio" id="folio"  class="form-control" readonly>
                                </div>
                            </div>

                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Empresas:</label>
                                <select name="empreusu" id="empreusu" class="form-control">
                                    <?php foreach ($empresas as $empresa) : ?>
                                        <option value="<?php echo $empresa->idEmpresa ?>"><?php echo $empresa->nombreEmpresa; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Areas:</label>
                                <select name="areausu" id="areausu" class="form-control">
                                    <?php foreach ($areas as $area) : ?>
                                        <option value="<?php echo $area->idArea ?>"><?php echo $area->nombreArea; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label> Imagen:</label>
                                <input type="file" class="form-control" id="imagen" name="imagen">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <label>Permisos:</label><br>
                                    <?php foreach ($permisos as $permiso) : ?>
                                    <input type="checkbox" class="form-check-input" name="permiso[]" value="<?php echo $permiso->idpermiso;?>"> <?php echo $permiso->permiso; ?><br>
                                    <?php endforeach; ?>
                                
                            </div>
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-success"><span class="fa fa-save"> Guardar</span></button>
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>usuarios/usuarios"><span class="fa fa-arrow-circle-left"> Cancelar</span></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->