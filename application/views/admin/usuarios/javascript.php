<script>
	var base_url = "<?php echo base_url(); ?>";
	$("#perfilusu").on("change", function() {
		id = $(this).val();
		$.ajax({
			type: "POST",
			url: base_url + "usuarios/usuarios/BuscarNumero",
			//      dataType: 'json',
			data: {
				"id": id
			},
			success: function(data) {
				//data = JSON.parse(data);
				//alert(data.folio);
				$('#folio').val(data.replace(/['"]+/g, ''));
			}
		}).fail(function(xhr, status, error) {
			console.log(xhr);
			console.log(status);
			console.log(error);
		})
	});

	function activarydesactivar(id,estatus) {
		swal({
				title: "Mensaje",
				text: "¿Deseas activar/desactivar este usuario?!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Si, acepto!",
				cancelButtonText: "No, cancelar!",
				closeOnConfirm: false
			},
			function() {
				//swal("Deleted!", "Your imaginary file has been deleted.", "success");
				$.ajax({
					type: "POST",
					url: base_url + "usuarios/usuarios/activarydesactivar",
					data: {
						"id": id,"estatus":estatus
					},
					success: function(data) {
						window.location.href = base_url + "usuarios/usuarios";
					}
				}).fail(function(xhr, status, error) {
					console.log(xhr);
					console.log(status);
					console.log(error);
				})
			});
	}
</script>
